## Polling manager

This section describes the TagItSmart Polling Manager app, which is an accompany app that enables the integration of the different authentication components such as BehavAuth, location based authentication (provided by the University of Surrey) and keystroke based authentication (provided by the University of Padova).


The Polling Manager operates mainly in an opportunistic manner. This means that the users just need to install the mobile app. After the installation process, the mobile app operates on its own without the needs of the users input. From the point that the app will be installed, it will monitor the communication bus about any incoming messages and in case it receives any messages it will forward them to the authentication voting mechanism and the result of it will be propagated to the User Repository of the TagItSmart platform.

---

## Installation

Follow the instructions below to install the app.

1. Download the .apk from [here](https://bitbucket.org/quadible/polling-manager/src/385be01bafdcdff5896bbc0bded00de2f2e738cd/app/app-release.apk?at=master) .
2. Click on the download notification to start installation.

---

## Permissions

These are the permissions required for the app.

1. 	Start the background service when the device boot is completed.

```
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
```

2.  Enable the app to post the authentication result to the TagItSmart platform.
```
    <uses-permission android:name="android.permission.INTERNET" />
```

---

## SDK Requirements

The requirements for the Android SDK are provided below:

```
android {
    compileSdkVersion 27
    buildToolsVersion "27.0.3"
    defaultConfig {
        applicationId "com.quadible.tispollingmanager"
        minSdkVersion 21
        targetSdkVersion 27
        versionCode 1
        versionName "1.0"       
    }
}

```
---

## Sample code to communicate with the Polling Manager

Following you will find the sample code on how to publish the authentication result to the Polling Manager.

The sample code needs to be executed either in an Activity or in a Service.

```java

    private static final String TIS_INTENT_FILTER = "eu.tagitsmart.auth.pollingmanager";
    private static final String AUTH_DATA_DEVICEID = "deviceID";
    private static final String AUTH_DATA_EMAIL = "email";
    private static final String AUTH_DATA_PASSWORD = "password";
    private static final String AUTH_DATA_UPDATEUSER = "updateUser";
    private static final String AUTH_AUTHENTICATOR_ID = "authenticatorId";
    
    private void sendAuthenticationResultToPollingManager() {
        Intent intent = new Intent();
        intent.setAction(TIS_INTENT_FILTER);
        intent.putExtra(AUTH_DATA_DEVICEID, "ThisIsADeviceId");
        intent.putExtra(AUTH_DATA_EMAIL,"ThisIsAnEmail@domain.com");
        intent.putExtra(AUTH_DATA_PASSWORD,"ThisIsAPassword");
        intent.putExtra(AUTH_DATA_UPDATEUSER,"ThisIsAUserName");
        intent.putExtra(AUTH_AUTHENTICATOR_ID, "ThisIsYourAuthenticatorName");
        sendBroadcast(intent);
    }
```


---

## Acknowledgements
The **Polling Manager** is part of the [TagItSmart EU project (688061)](www.tagitsmart.eu) that was funded by European Union's Horizon 2020 Research and Innovation Programme.

---

## License

```
Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
funded by European Union's Horizon 2020 Research and Innovation Programme.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quadible.tispollingmanager.utils;

import com.quadible.tispollingmanager.models.AuthData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VotingMechanism {

    private List<AuthData> authDataList;

    public VotingMechanism () {
        authDataList = new ArrayList<>();
    }

    public void add(AuthData data) {
        for (int i = 0; i < authDataList.size(); i++) {
            if (authDataList.get(i).getAuthenticatorId().contains(data.getAuthenticatorId())) {
                authDataList.remove(i);
            }
        }
        authDataList.add(data);
    }

    public AuthData computeAuthResult() {
        AuthData data = new AuthData();
        List<String> userList = new ArrayList<>();

        if (authDataList.isEmpty()) return null;

        // Store temporary the results of the authenticators
        for (AuthData d : authDataList) {
            userList.add(d.getUpdateUser());
        }

        // Find the most popular result
        String resultUser = mostCommon(userList);

        // Retrieve all the authnetication data for the most popular result
        for (AuthData d : authDataList) {
            if (d.getUpdateUser().contains(resultUser)) {
                data = d;
                break;
            }
        }

        return data;
    }

    public void clear() {
        authDataList.clear();
    }

    private static <T> T mostCommon(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<T, Integer> max = null;

        for (Map.Entry<T, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue())
                max = e;
        }

        return max.getKey();
    }
}

/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quadible.tispollingmanager.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.quadible.tispollingmanager.models.AuthData;
import com.quadible.tispollingmanager.utils.TISUserRepositoryController;
import com.quadible.tispollingmanager.utils.VotingMechanism;

public class BroadcastReceiverService extends Service {

    private static final String TIS_INTENT_FILTER = "eu.tagitsmart.auth.pollingmanager";

    private static long PERIOD = 1000;
    public static final String ACTION_UI = TIS_INTENT_FILTER + ".services.BroadcastReceiverService.ACTION_UI";
    public static final String EXTRA_POLLING_PERIOD = TIS_INTENT_FILTER + ".services.BroadcastReceiverService.EXTRA_POLLING_PERIOD";

    private TISBroadcastReceiver mBroadcastReceiver;
    private TISUserRepositoryController mController;
    private Handler mHandler;
    private volatile static VotingMechanism mVotingMechanism = new VotingMechanism();

    @Override
    public void onCreate() {
        mBroadcastReceiver = new TISBroadcastReceiver();
        mController = new TISUserRepositoryController();

        mHandler = new Handler();
        mHandler.post(new TISCommunicationRunnable());
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        // Register the broadcastReceiver
        IntentFilter filter = new IntentFilter(TIS_INTENT_FILTER);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        this.registerReceiver(mBroadcastReceiver, filter);

        IntentFilter uiIntentFilter = new IntentFilter(ACTION_UI);
        LocalBroadcastManager.getInstance(this).registerReceiver(mUIBroadcastReceiver, uiIntentFilter);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // Stop receiving messages from authenticators
        this.unregisterReceiver(mBroadcastReceiver);

        if (mUIBroadcastReceiver != null) LocalBroadcastManager.getInstance(this).unregisterReceiver(mUIBroadcastReceiver);

        // Stop merging authentication results and sending them to platform
        mHandler.removeCallbacks(null);
    }

    private BroadcastReceiver mUIBroadcastReceiver = new BroadcastReceiver() {
        public static final String TAG = "mUIBroadcastReceiver";

        @Override
        public void onReceive( Context context, Intent intent ) {
            int pollingPeriod = intent.getIntExtra(EXTRA_POLLING_PERIOD, -1);

            if (pollingPeriod > 1) {
                PERIOD = pollingPeriod * 1000;
                Log.i(TAG, "Polling period changed to " + PERIOD + " sec");
            }
        }
    };

    public static class TISBroadcastReceiver extends BroadcastReceiver {

        private static final String TAG = "TISBroadcastReceiver";
        private static final String AUTH_DATA_DEVICEID = "deviceID";
        private static final String AUTH_DATA_EMAIL = "email";
        private static final String AUTH_DATA_PASSWORD = "password";
        private static final String AUTH_DATA_UPDATEUSER = "updateUser";
        private static final String AUTH_AUTHENTICATOR_ID = "authenticatorId";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceived(): msg ID: " + intent.getLongExtra("id", -1));

            //AuthData data = (AuthData) intent.getSerializableExtra(AUTH_DATA);
            AuthData data = new AuthData();
            data.setDeviceID(intent.getStringExtra(AUTH_DATA_DEVICEID));
            data.setEmail(intent.getStringExtra(AUTH_DATA_EMAIL));
            data.setPassword(intent.getStringExtra(AUTH_DATA_PASSWORD));
            data.setUpdateUser(intent.getStringExtra(AUTH_DATA_UPDATEUSER));
            data.setAuthenticatorId(intent.getStringExtra(AUTH_AUTHENTICATOR_ID));

            mVotingMechanism.add(data);
        }
    }

    private class TISCommunicationRunnable implements Runnable {

        @Override
        public void run() {
            Log.i("TISCommunicationRunnable", "run()");
            AuthData data = mVotingMechanism.computeAuthResult();
            if (data != null) {
                mController.updateAuth(data);

                mVotingMechanism.clear();
            }
            mHandler.postDelayed(new TISCommunicationRunnable(), PERIOD);
        }
    }
}
